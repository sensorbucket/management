CREATE EXTENSION IF NOT EXISTS "postgis";

CREATE TABLE "device_type"
(
  "id"                  VARCHAR     PRIMARY KEY NOT NULL,
  "name"                VARCHAR(50) NOT NULL,
  "description"         VARCHAR(75) NOT NULL,
  "mobile"              BOOLEAN     NOT NULL,
  "configurationSchema" JSON        NOT NULL
);

CREATE TABLE "source"
(
  "id"                  VARCHAR       PRIMARY KEY NOT NULL,
  "name"                VARCHAR(50)   NOT NULL,
  "description"         VARCHAR(128)  NOT NULL,
  "configurationSchema" JSON          NOT NULL
);

CREATE TABLE "location"
(
  "id"              SERIAL        PRIMARY KEY,
  "name"            VARCHAR(50)   NOT NULL,
  "ownerId"           INTEGER       NOT NULL,
  "description"     VARCHAR(75)   NOT NULL,
  "address"         VARCHAR(50),
  "city"            VARCHAR(50),
  "coordinates"     GEOMETRY      NOT NULL
);


CREATE TABLE "device"
(
  "id"            SERIAL        PRIMARY KEY,
  "code"          VARCHAR(75)   NOT NULL,
  "description"   VARCHAR(128)  NOT NULL,
  "typeId"        VARCHAR       NOT NULL,
  "sourceId"      VARCHAR       NOT NULL,
  "ownerId"         INTEGER       NOT NULL,
  "locationId"    INTEGER,
  "configuration" JSON          NOT NULL,

  CONSTRAINT "FK_device_type"
    FOREIGN KEY("typeId")
      REFERENCES "device_type"("id"),
  CONSTRAINT "FK_device_source"
    FOREIGN KEY("sourceId")
      REFERENCES "source"("id"),
  CONSTRAINT "FK_device_location"
    FOREIGN KEY("locationId")
      REFERENCES "location"("id")
);

CREATE TABLE "measurement_type"
(
  "id"            SERIAL      PRIMARY KEY,
  "name"          VARCHAR(25) NOT NULL,
  "description"   VARCHAR(75) NOT NULL,
  "unit"          VARCHAR(12) NOT NULL
);