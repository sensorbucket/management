import { Test, TestingModule } from '@nestjs/testing';
import { ConfigService } from '@nestjs/config';
import * as request from 'supertest';
import { AppModule } from 'src/app.module';
import { ConfigFixture, cleanDatabase, loadFixtures } from './fixtures';
import { CreateDeviceDTO } from 'src/device/dto/createDevice.dto';
import { UpdateDeviceDTO } from 'src/device/dto/updateDevice.dto';

describe('Devices (e2e)', () => {
  let app;
  let agent: request.SuperTest<request.Test>;

  //
  // Fixtures
  //
  const deviceDTO: CreateDeviceDTO = {
    name: 'NewDevice',
    owner: 0,
    description: 'A newly created device',
    source: 'ttn',
    type: 'first-device-type',
    configuration: {},
  };
  const updateDeviceDTO: UpdateDeviceDTO = {
    description: 'An updated description!',
  };

  //
  // Tests
  //

  beforeAll(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    })
      .overrideProvider(ConfigService)
      .useValue(ConfigFixture)
      .compile();

    app = moduleFixture.createNestApplication();
    await app.init();

    await loadFixtures(__dirname + '/fixtures');

    agent = request.agent(app.getHttpServer());
  });

  afterAll(async () => {
    await cleanDatabase();
    await app.close();
  });

  let createdEntityId: number;
  /**
   *
   */
  it('Should create new Device', async () => {
    return agent
      .post('/devices')
      .send(deviceDTO)
      .expect(201)
      .expect((res) => {
        expect(res.body).toHaveProperty('id', expect.any(Number));
        createdEntityId = res.body.id;
        expect(res.body).toHaveProperty('name', deviceDTO.name);
        expect(res.body).toHaveProperty('sourceId', deviceDTO.source);
        expect(res.body).toHaveProperty('typeId', deviceDTO.type);
      });
  });

  /**
   *
   */
  it('Get return created Device', async () => {
    return agent
      .get(`/devices/${createdEntityId}`)
      .send()
      .expect(200)
      .expect((res) => {
        expect(res.body).toHaveProperty('id', createdEntityId);
        expect(res.body).toHaveProperty('name', deviceDTO.name);
        expect(res.body).toHaveProperty('sourceId', deviceDTO.source);
        expect(res.body).toHaveProperty('typeId', deviceDTO.type);
      });
  });

  /**
   *
   */
  describe('Should patch Device', () => {
    it('update Device', async () => {
      return agent
        .patch('/devices/1')
        .send(updateDeviceDTO)
        .expect(200)
        .expect((res) => {
          expect(res.body).toMatchObject(updateDeviceDTO);
        });
    });
    it('get and verify updated', async () => {
      return agent
        .get('/devices/1')
        .send()
        .expect(200)
        .expect((res) => {
          expect(res.body).toMatchObject(updateDeviceDTO);
        });
    });
  });

  /**
   *
   */
  it.skip('Get Device owned by joined organisations', async () => {});

  /**
   *
   */
  describe('Should delete Device', () => {
    it('delete Device', async () => {
      return agent.delete(`/devices/${createdEntityId}`).send().expect(200);
    });
    it('expect 404', async () => {
      return agent.get(`/devices/${createdEntityId}`).send().expect(404);
    });
  });

  it('Should throw NotFound on non-existing Device', async () => {
    return agent.get('/devices/99999').send().expect(404);
  });
});
