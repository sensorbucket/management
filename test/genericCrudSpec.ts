import * as request from 'supertest';

interface GenericSpecE2EOptions {
  getAgent: () => request.SuperTest<request.Test>;
  entityName: string;
  baseUrl: string;
  createDTO: {};
  patchDTO: {};
}

export const GenericCrudSpecE2E = ({
  getAgent,
  baseUrl,
  createDTO,
  entityName,
  patchDTO,
}: GenericSpecE2EOptions) => () => {
  let createdEntityId: number;
  /**
   *
   */
  it(`Should create new ${entityName}`, async () => {
    return getAgent()
      .post(baseUrl)
      .send(createDTO)
      .expect(201)
      .expect((res) => {
        expect(res.body).toHaveProperty('id', expect.any(Number));
        createdEntityId = res.body.id;
        expect(res.body).toMatchObject(createDTO);
      });
  });

  /**
   *
   */
  it(`Get return created ${entityName}`, async () => {
    return getAgent()
      .get(`${baseUrl}/${createdEntityId}`)
      .send()
      .expect(200)
      .expect((res) => {
        expect(res.body).toMatchObject(createDTO);
      });
  });

  /**
   *
   */
  describe(`Should patch ${entityName}`, () => {
    it(`update ${entityName}`, async () => {
      return getAgent()
        .patch(`${baseUrl}/1`)
        .send(patchDTO)
        .expect(200)
        .expect((res) => {
          expect(res.body).toMatchObject(patchDTO);
        });
    });
    it(`get and verify updated`, async () => {
      return getAgent()
        .get(`${baseUrl}/1`)
        .send()
        .expect(200)
        .expect((res) => {
          expect(res.body).toMatchObject(patchDTO);
        });
    });
  });

  /**
   *
   */
  it.skip(`Get ${entityName} owned by joined organisations`, async () => {});

  /**
   *
   */
  describe(`Should delete ${entityName}`, () => {
    it(`delete ${entityName}`, async () => {
      return getAgent()
        .delete(`${baseUrl}/${createdEntityId}`)
        .send()
        .expect(200);
    });
    it(`expect 404`, async () => {
      return getAgent().get(`${baseUrl}/${createdEntityId}`).send().expect(404);
    });
  });

  it(`Should throw NotFound on non-existing ${entityName}`, async () => {
    return getAgent().get(`${baseUrl}/99999`).send().expect(404);
  });
};
