import { IsJSON, IsString, MaxLength, MinLength, IsOptional } from 'class-validator';
import { IsJSONSchema } from '../../common/validators';

export class UpdateSourceDTO {
  @IsString()
  @MaxLength(75)
  @MinLength(3)
  @IsOptional()
  name?: string;

  @IsString()
  @MaxLength(128)
  @IsOptional()
  description: string;

  @IsJSON()
  @IsJSONSchema()
  @IsOptional()
  configurationSchema?: string | { [index: string]: any };
}
