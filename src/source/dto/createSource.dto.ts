import { IsString, MaxLength, MinLength, IsJSON, IsOptional } from 'class-validator';
import { IsJSONSchema } from '../../common/validators';

export class CreateSourceDTO {
  @IsString()
  @MaxLength(75)
  @MinLength(3)
  @IsOptional()
  id?: string;

  @IsString()
  @MaxLength(75)
  @MinLength(3)
  name: string;

  @IsString()
  @MaxLength(128)
  @IsOptional()
  description: string;

  @IsJSON()
  @IsJSONSchema()
  configurationSchema: string | { [index: string]: any };
}
