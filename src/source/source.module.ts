import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { SourceService } from './source.service';
import { SourceController } from './source.controller';
import { Source } from './entities/source.entity';
import { SourceAuth } from './source.auth';
import { AuthModule } from '../common/authentication/auth.module';

@Module({
  imports: [AuthModule, TypeOrmModule.forFeature([Source])],
  providers: [SourceAuth, SourceService],
  controllers: [SourceController],
  exports: [SourceService],
})
export class SourceModule {}
