import { Entity, Column, PrimaryColumn } from 'typeorm';

@Entity()
export class Source {
  @PrimaryColumn()
  id: string;

  @Column({ length: 50 })
  name: string;

  @Column({ length: 128 })
  description: string;

  @Column({ type: 'json' })
  configurationSchema: {};
}
