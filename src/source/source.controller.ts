import {
  Controller,
  Get,
  Param,
  NotFoundException,
  Post,
  Body,
  Patch,
  Delete,
  ParseIntPipe,
  BadRequestException,
  UnauthorizedException,
  UseGuards,
} from '@nestjs/common';
import { SourceService } from './source.service';
import { CreateSourceDTO } from './dto/createSource.dto';
import { UpdateSourceDTO } from './dto/updateSource.dto';
import { ReqToken } from '../common/authentication/decorators';
import { Token } from '../common/authentication/token';
import { AuthGuard } from '../common/authentication/guards';
import { UserToken } from '../common/authentication/user.token';
import { SourceAuth } from './source.auth';
import { ServiceToken } from '../common/authentication/service.token';

@Controller('sources')
export class SourceController {
  constructor(private readonly service: SourceService, private readonly auth: SourceAuth) {}

  @Get()
  @UseGuards(new AuthGuard([UserToken, ServiceToken]))
  getMany() {
    return this.service.getMany();
  }

  @Get(':id')
  @UseGuards(new AuthGuard([UserToken, ServiceToken]))
  async getOne(@Param('id') id: string) {
    const source = await this.service.getOne(id);
    if (!source) {
      throw new NotFoundException();
    }
    return source;
  }

  @Post()
  @UseGuards(new AuthGuard([UserToken]))
  createOne(@ReqToken() token: Token, @Body() dto: CreateSourceDTO) {
    this.auth.canCreate(token);
    return this.service.createOne(dto);
  }

  @Patch(':id')
  @UseGuards(new AuthGuard([UserToken]))
  async updateOne(@ReqToken() token: Token, @Param('id') id: string, @Body() dto: UpdateSourceDTO) {
    this.auth.canUpdate(token);
    const source = await this.service.getOne(id);
    if (!source) {
      throw new NotFoundException();
    }
    return this.service.updateOne(id, dto);
  }

  @Delete(':id')
  @UseGuards(new AuthGuard([UserToken]))
  async deleteOne(@ReqToken() token: Token, @Param('id') id: string) {
    this.auth.canDelete(token);
    const source = await this.service.getOne(id);
    if (!source) {
      throw new NotFoundException();
    }
    return this.service.deleteOne(id);
  }

  @Post(':id/token')
  @UseGuards(new AuthGuard([UserToken]))
  async createToken(@ReqToken() token: Token, @Param('id') id: string, @Body('owner', ParseIntPipe) owner: number) {
    this.auth.canCreateToken(token, owner);
    const sourceToken = await this.service.createToken(id, owner);
    return {
      message: 'Created source token',
      data: sourceToken,
    };
  }
}
