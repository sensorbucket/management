import { Test, TestingModule } from '@nestjs/testing';
import { SourceService } from './source.service';
import { getRepositoryToken } from '@nestjs/typeorm';
import { Source } from './entities/source.entity';

const repositoryMock = {};

describe('SourceService', () => {
  let service: SourceService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        {
          provide: getRepositoryToken(Source),
          useValue: repositoryMock,
        },
        SourceService,
      ],
    }).compile();

    service = module.get<SourceService>(SourceService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
