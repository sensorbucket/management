import { ForbiddenException, Injectable, UnauthorizedException } from '@nestjs/common';
import { Token } from '../common/authentication/token';

@Injectable()
export class SourceAuth {
  canCreate(token: Token) {
    if (!token.isSuperUser()) {
      throw new ForbiddenException();
    }
  }

  canUpdate(token: Token) {
    if (!token.isSuperUser()) {
      throw new ForbiddenException();
    }
  }

  canDelete(token: Token) {
    if (!token.isSuperUser()) {
      throw new ForbiddenException();
    }
  }

  canCreateToken(token: Token, organisationId: number) {
    if (!token.canAccessOrganisation(organisationId)) {
      throw new UnauthorizedException('Cannot create a source token for this organisation');
    }
  }
}
