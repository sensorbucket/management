import { BadRequestException, Injectable, Logger } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import * as Ajv from 'ajv';
import * as JWT from 'jsonwebtoken';
import { Repository } from 'typeorm';
import { Keystore } from '../common/authentication/keystore';
import { AppConfig } from '../config/config.service';
import { CreateSourceDTO } from './dto/createSource.dto';
import { UpdateSourceDTO } from './dto/updateSource.dto';
import { Source } from './entities/source.entity';

@Injectable()
export class SourceService {
  private readonly logger = new Logger(SourceService.name, true);
  private readonly ajv: Ajv.Ajv;
  private readonly repo: Repository<Source>;
  private readonly keys: Keystore;
  private readonly config: AppConfig;

  constructor(@InjectRepository(Source) repo: Repository<Source>, config: AppConfig, keys: Keystore) {
    this.ajv = Ajv();
    this.repo = repo;
    this.config = config;
    this.keys = keys;
  }

  /**
   * Test if a given object is a valid JSON schema
   * @param schema
   */
  validateConfigurationSchema(schema: {}): Ajv.ErrorObject[] {
    this.ajv.validateSchema(schema);
    return this.ajv.errors;
  }

  /**
   * Get all existing sources
   */
  async getMany(): Promise<Source[]> {
    return this.repo.createQueryBuilder().select().getMany();
  }

  /**
   * Get a source by its ID
   * @param id the ID
   */
  async getOne(id: string): Promise<Source> {
    return this.repo.createQueryBuilder().select().where('id = :id', { id }).getOne();
  }

  /**
   * Get Create a new Source
   * @param _dto Object containing source information
   */
  async createOne(_dto: CreateSourceDTO): Promise<Source> {
    const dto = { ..._dto };
    // ConfigurationSchema must be a valid Draft #7 JSON Schema
    const errors = this.validateConfigurationSchema(dto.configurationSchema);
    if (errors && errors.length > 0) {
      throw new BadRequestException({ errors }, 'ConfigurationSchema is not a valid JSON Schema');
    }

    if (!dto.id) {
      dto.id = dto.name
        .toLocaleLowerCase()
        .replace(/[^a-z0-9\s]/i, '')
        .replace(' ', '-');
    }

    // No duplicate name allowed
    const duplicate =
      (await this.repo.createQueryBuilder().select().where('name = :name', { name: dto.name }).getCount()) > 0;
    if (duplicate) {
      throw new BadRequestException('Source with that name already exists');
    }

    // Create source
    const result = await this.repo
      .createQueryBuilder()
      .insert()
      .values([dto as {}])
      .returning('*')
      .execute();
    return this.getOne(result.generatedMaps[0].id);
  }

  /**
   * Update an existing source
   * @param source The source to update
   * @param dto The modified source properties
   */
  async updateOne(id: string, dto: UpdateSourceDTO): Promise<Source> {
    await this.repo
      .createQueryBuilder()
      .update()
      .set(dto as {})
      .where('id = :id', { id })
      .execute();
    return this.getOne(id);
  }

  /**
   * Delete a source
   * @param id The source its ID
   */
  async deleteOne(id: string): Promise<void> {
    const result = await this.repo.createQueryBuilder().delete().where('id = :id', { id }).execute();
  }

  /**
   * Generate a source authentication token for an organisation
   * @param source The source its id
   * @param org The organisation its id
   */
  async createToken(source: string, org: number): Promise<string> {
    // Create token
    const options = <JWT.SignOptions>{
      subject: source,
      expiresIn: this.config.jwtExpiresIn,
      issuer: 'sensorbucket',
      audience: 'source',
      algorithm: 'RS256',
    };
    const payload = {
      org,
    };

    return await this.signToken(payload, options);
  }

  /**
   *
   * @param payload
   * @param options
   */
  private async signToken(payload: object, options: JWT.SignOptions): Promise<string> {
    return new Promise(async (resolve, reject) => {
      // Retrieve key at index 0
      const key = await this.keys.getLatestPEM();
      options.keyid = key.kid;

      // Create and sign token
      JWT.sign(payload, key.pem, options, (err, token) => {
        if (err) {
          return reject(err);
        }
        resolve(token);
      });
    });
  }
}
