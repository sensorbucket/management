import { BadRequestException, Injectable, NotFoundException, UnauthorizedException } from '@nestjs/common';
import { ServiceToken } from '../common/authentication/service.token';
import { SourceToken } from '../common/authentication/source.token';
import { Token } from '../common/authentication/token';
import { UserToken } from '../common/authentication/user.token';
import { Location } from './entities/location.entity';

@Injectable()
export class LocationAuth {
  canGetMany(token: Token) {
    //
  }

  canGetOne(token: Token, location: Location) {
    if (!token.canAccessOrganisation(location.ownerId)) {
      throw new NotFoundException();
    }
  }

  canCreateOne(token: Token, owner: number) {
    if (!token.canAccessOrganisation(owner)) {
      throw new BadRequestException('Cannot create location for that organisation');
    }
  }

  canUpdateOne(token: Token, location: Location) {
    if (!token.canAccessOrganisation(location.ownerId)) {
      throw new NotFoundException();
    }
  }

  canDeleteOne(token: Token, location: Location) {
    if (!token.canAccessOrganisation(location.ownerId)) {
      throw new NotFoundException();
    }
  }
}
