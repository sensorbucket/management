import { Test, TestingModule } from '@nestjs/testing';
import { LocationService } from './location.service';
import { UpdateLocationDTO } from './dto/updateLocation.dto';
import { getRepositoryToken } from '@nestjs/typeorm';
import { Location } from './entities/location.entity';
import { plainToClass } from 'class-transformer';
import { BadRequestException } from '@nestjs/common';

const Repository = {
  create: (obj: any) => plainToClass(Location, obj),
  save: jest.fn(),
  update: jest.fn(),
};

describe('LocationService', () => {
  let service: LocationService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        {
          provide: getRepositoryToken(Location),
          useValue: Repository,
        },
        LocationService,
      ],
    }).compile();

    service = module.get<LocationService>(LocationService);
  });

  it('should update name and or description', async () => {
    //
  });

  it('should not update with empty dto', async () => {
    //
  });

  it('should not update if user is not member of organisation', async () => {
    //
  });

  it('should create a location', async () => {
    //
  });

  it('should not create location if user is not member of organisation', async () => {
    //
  });

  it('should delete location', async () => {
    //
  });

  it('should not delete location if user is not member of organisation', async () => {
    //
  });
});
