import { Entity, PrimaryGeneratedColumn, Column } from 'typeorm';

@Entity()
export class Location {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ length: 50 })
  name: string;

  // External reference to Organisation
  @Column()
  ownerId: number;

  @Column({ length: 75 })
  description: string;

  @Column({ length: 50, nullable: true })
  address: string;

  @Column({ length: 50, nullable: true })
  city: string;

  @Column({
    type: 'geometry',
    srid: 4326,
  })
  coordinates: any;
}
