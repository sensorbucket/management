import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Location } from './entities/location.entity';
import { LocationService } from './location.service';
import { LocationController } from './location.controller';
import { LocationAuth } from './location.auth';

@Module({
  imports: [TypeOrmModule.forFeature([Location])],
  providers: [LocationAuth, LocationService],
  controllers: [LocationController],
  exports: [LocationService],
})
export class LocationModule {}
