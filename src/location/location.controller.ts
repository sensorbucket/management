import {
  Controller,
  Get,
  Param,
  ParseIntPipe,
  Post,
  Body,
  UsePipes,
  ValidationPipe,
  Patch,
  Delete,
  NotFoundException,
  UseGuards,
} from '@nestjs/common';
import { LocationService } from './location.service';
import { CreateLocationDTO } from './dto/createLocation.dto';
import { UpdateLocationDTO } from './dto/updateLocation.dto';
import { APIResponse } from '../common/response';
import { AuthGuard } from '../common/authentication/guards';
import { UserToken } from '../common/authentication/user.token';
import { SourceToken } from '../common/authentication/source.token';
import { ReqToken } from '../common/authentication/decorators';
import { Token } from '../common/authentication/token';
import { LocationAuth } from './location.auth';
import { ServiceToken } from '../common/authentication/service.token';

@UsePipes(new ValidationPipe({ whitelist: true }))
@Controller('locations')
export class LocationController {
  constructor(private readonly service: LocationService, private readonly auth: LocationAuth) {}

  @Get()
  @UseGuards(new AuthGuard([UserToken, SourceToken, ServiceToken]))
  async getMany(@ReqToken() token: Token) {
    // TODO: Add filtering and filter on token access (owner id)
    let locations = await this.service.getMany();
    return new APIResponse(200, 'Fetched locations', locations);
  }

  @Get(':id')
  @UseGuards(new AuthGuard([UserToken, SourceToken, ServiceToken]))
  async getOne(@ReqToken() token: Token, @Param('id', ParseIntPipe) id: number) {
    const location = await this.service.getOne(id);
    if (!location) {
      throw new NotFoundException();
    }
    this.auth.canGetOne(token, location);
    return location;
  }

  @Post()
  @UseGuards(new AuthGuard([UserToken]))
  createOne(@ReqToken() token: Token, @Body() dto: CreateLocationDTO) {
    this.auth.canCreateOne(token, dto.ownerId);
    return this.service.createOne(dto);
  }

  @Patch(':id')
  @UseGuards(new AuthGuard([UserToken]))
  async updateOne(@ReqToken() token: Token, @Param('id', ParseIntPipe) id: number, @Body() dto: UpdateLocationDTO) {
    const location = await this.service.getOne(id);
    if (!location) {
      throw new NotFoundException();
    }
    this.auth.canUpdateOne(token, location);
    return this.service.updateOne(location, dto);
  }

  @Delete(':id')
  @UseGuards(new AuthGuard([UserToken]))
  async deleteOne(@ReqToken() token: Token, @Param('id', ParseIntPipe) id: number) {
    const location = await this.service.getOne(id);
    if (!location) {
      throw new NotFoundException();
    }
    this.auth.canDeleteOne(token, location);
    return this.service.deleteOne(location);
  }
}
