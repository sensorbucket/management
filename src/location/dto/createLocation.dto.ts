import { IsNumber, IsString, IsUUID, MaxLength, ValidateNested } from 'class-validator';
import { Type } from 'class-transformer';
import { GeoJSON } from './geoJson';

/**
 * DTO for creating a new location
 */
export class CreateLocationDTO {
  @IsString()
  @MaxLength(50)
  name: string;

  @IsString()
  @MaxLength(255)
  description: string;

  @IsNumber()
  ownerId: number;

  @ValidateNested()
  @Type(() => GeoJSON)
  coordinates: GeoJSON;
}
