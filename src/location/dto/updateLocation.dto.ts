import { IsUUID, IsOptional, IsString, MaxLength, ValidateNested } from 'class-validator';
import { GeoJSON } from './geoJson';
import { Type } from 'class-transformer';

export class UpdateLocationDTO {
  @IsUUID()
  @IsOptional()
  organisationId: string;

  @IsString()
  @MaxLength(50)
  @IsOptional()
  name: string;

  @IsString()
  @MaxLength(255)
  @IsOptional()
  description: string;

  @ValidateNested()
  @Type(() => GeoJSON)
  @IsOptional()
  coordinates: GeoJSON;
}
