import {
  IsNumber,
  IsString,
  ArrayMaxSize,
  ArrayMinSize,
  Equals,
} from 'class-validator';
/**
 * Represents GeoJSON format
 */
export class GeoJSON {
  @IsString()
  @Equals('Point')
  type: 'Point';

  @IsNumber({}, { each: true })
  @ArrayMaxSize(2)
  @ArrayMinSize(2)
  coordinates: number[];
}
