import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { APIResponseInterceptor } from './common/interceptors';
import { HttpExceptionFilter } from './common/filters/exception.filter';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  app.useGlobalFilters(new HttpExceptionFilter());
  app.useGlobalInterceptors(new APIResponseInterceptor());
  await app.listen(3000);
}
bootstrap();
