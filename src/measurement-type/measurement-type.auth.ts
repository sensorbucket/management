import { ForbiddenException, Injectable } from '@nestjs/common';
import { Token } from '../common/authentication/token';

@Injectable()
export class MeasurementTypeAuth {
  canCreateOne(token: Token) {
    if (!token.isSuperUser()) {
      throw new ForbiddenException();
    }
  }

  canDeleteOne(token: Token) {
    if (!token.isSuperUser()) {
      throw new ForbiddenException();
    }
  }
}
