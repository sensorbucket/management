import { Injectable, MethodNotAllowedException, NotImplementedException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { PageMetadata, QueryFilter } from '../common/decorators';
import { CreateMeasurementTypeDTO } from './dto/create-measurement-type.dto';
import { MeasurementType } from './entities/measurement-type.model';

export interface MeasurementTypePage {
  metadata: PageMetadata;
  measurementTypes: MeasurementType[];
}

@Injectable()
export class MeasurementTypeService {
  constructor(@InjectRepository(MeasurementType) private readonly repo: Repository<MeasurementType>) {}

  async getOne(id: number): Promise<MeasurementType> {
    return this.repo.createQueryBuilder().select().where('id = :id', { id }).getOne();
  }

  async getPage(qf: QueryFilter): Promise<MeasurementTypePage> {
    // Implement some function that applies the QueryFilter to the
    // typeorm querybuilder
    const units = await this.repo
      .createQueryBuilder()
      .select()
      .limit(qf.limit)
      .offset(qf.page * qf.limit)
      .where(qf.filters)
      .getMany();

    // TODO: Should paging metadata be given here? We could do it in the service
    // since it also has de QF data. However, we wouldn't know if it is the last
    // page...
    return {
      metadata: {
        limit: qf.limit,
        page: qf.page,
      },
      measurementTypes: units,
    };
  }

  async create(dto: CreateMeasurementTypeDTO): Promise<MeasurementType> {
    const res = await this.repo.createQueryBuilder().insert().values(dto).returning('*').execute();

    return this.getOne(res.generatedMaps[0].id);
  }

  async delete(id: number) {
    // Should measure units be removable? Perhaps it's easier to block the request
    // instead of having the DB check if there is any Foreign Key relation.
    throw new MethodNotAllowedException();
  }
}
