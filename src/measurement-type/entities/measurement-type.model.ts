import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class MeasurementType {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ length: 25 })
  name: string;

  @Column({ length: 75 })
  description: string;

  @Column({ length: 12 })
  unit: string;
}
