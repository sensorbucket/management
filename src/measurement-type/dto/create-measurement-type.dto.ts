import { IsString, MaxLength } from 'class-validator';

export class CreateMeasurementTypeDTO {
  @IsString()
  @MaxLength(25)
  name: string;

  @IsString()
  @MaxLength(75)
  description: string;

  @IsString()
  @MaxLength(12)
  unit: string;
}
