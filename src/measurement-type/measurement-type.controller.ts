import {
  Body,
  Controller,
  Delete,
  Get,
  MethodNotAllowedException,
  NotFoundException,
  Param,
  ParseIntPipe,
  Post,
  UseGuards,
} from '@nestjs/common';
import { ReqToken } from '../common/authentication/decorators';
import { AuthGuard } from '../common/authentication/guards';
import { ServiceToken } from '../common/authentication/service.token';
import { Token } from '../common/authentication/token';
import { UserToken } from '../common/authentication/user.token';
import { CreateQueryFilterExtractor, QueryFilter } from '../common/decorators';
import { APIPaginatedResponse, APIResponse } from '../common/response';
import { CreateMeasurementTypeDTO } from './dto/create-measurement-type.dto';
import { MeasurementTypeAuth } from './measurement-type.auth';
import { MeasurementTypeService } from './measurement-type.service';

const QueryFilterExtractor = CreateQueryFilterExtractor({
  filterFields: ['valueName'],
});

@Controller('measurement-types')
export class MeasurementTypeController {
  constructor(private readonly service: MeasurementTypeService, private readonly auth: MeasurementTypeAuth) {}

  @Get('/:id')
  @UseGuards(new AuthGuard([UserToken, ServiceToken]))
  async getOne(@Param('id', ParseIntPipe) id: number) {
    const type = await this.service.getOne(id);
    if (!type) {
      throw new NotFoundException();
    }
    return new APIResponse(200, 'Succesfully fetched measurement type', type);
  }

  @Get('/')
  @UseGuards(new AuthGuard([UserToken, ServiceToken]))
  async getMany(@QueryFilterExtractor() qf: QueryFilter) {
    const page = await this.service.getPage(qf);
    return new APIPaginatedResponse(
      200,
      'Succesfully fetched page of measurement types',
      page.measurementTypes,
      page.metadata
    );
  }

  @Post('/')
  @UseGuards(new AuthGuard([UserToken]))
  async createOne(@ReqToken() token: Token, @Body() dto: CreateMeasurementTypeDTO) {
    this.auth.canCreateOne(token);
    const type = await this.service.create(dto);
    return new APIResponse(201, 'Succesfully created measurement type', type);
  }

  @Delete('/:id')
  @UseGuards(new AuthGuard([UserToken]))
  async deleteOne(@ReqToken() token: Token) {
    this.auth.canCreateOne(token);
    throw new MethodNotAllowedException();
  }
}
