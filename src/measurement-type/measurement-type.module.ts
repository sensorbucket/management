import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { MeasurementType } from './entities/measurement-type.model';
import { MeasurementTypeAuth } from './measurement-type.auth';
import { MeasurementTypeController } from './measurement-type.controller';
import { MeasurementTypeService } from './measurement-type.service';

@Module({
  imports: [TypeOrmModule.forFeature([MeasurementType])],
  providers: [MeasurementTypeAuth, MeasurementTypeService],
  controllers: [MeasurementTypeController],
  exports: [MeasurementTypeService],
})
export class MeasurementTypeModule {}
