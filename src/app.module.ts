import { MiddlewareConsumer, Module, NestModule } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { LocationModule } from './location/location.module';
import { ConfigModule } from './config/config.module';
import { AppConfig } from './config/config.service';
import { SourceModule } from './source/source.module';
import { DeviceTypeModule } from './device-type/device-type.module';
import { DeviceModule } from './device/device.module';
import { MeasurementTypeModule } from './measurement-type/measurement-type.module';
import { AuthModule } from './common/authentication/auth.module';
import { AuthenticationMiddleware } from './common/authentication/middleware';
@Module({
  imports: [
    ConfigModule,
    AuthModule,
    TypeOrmModule.forRootAsync({
      inject: [AppConfig],
      useFactory: async (config: AppConfig) => ({
        type: 'postgres',
        url: config.dbURL,
        synchronize: false,
        autoLoadEntities: true,
      }),
    }),
    LocationModule,
    DeviceTypeModule,
    DeviceModule,
    SourceModule,
    MeasurementTypeModule,
  ],
  controllers: [],
  providers: [],
})
export class AppModule implements NestModule {
  configure(consumer: MiddlewareConsumer) {
    consumer.apply(AuthenticationMiddleware).forRoutes('*');
  }
}
