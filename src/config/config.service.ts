import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';

@Injectable()
export class AppConfig {
  constructor(private readonly config: ConfigService) {}

  //
  //  Database configuration
  //

  get dbURL() {
    return this.config.get('DB_URL', 'postgres://localhost/management');
  }

  get dbSSL() {
    return this.config.get<string>('DB_SSL', 'FALSE').toUpperCase() === 'TRUE';
  }

  //
  // Token generation
  //

  get jwtExpiresIn() {
    return this.config.get('JWT_EXPIRES_IN', '15m');
  }

  get kmsSigning() {
    return this.config.get('JWKS_SIGNING', 'http://key-management/private/sensorbucket');
  }
}
