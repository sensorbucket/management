import {
  Controller,
  Get,
  Param,
  ParseIntPipe,
  NotFoundException,
  Post,
  Body,
  Patch,
  Delete,
  UsePipes,
  ValidationPipe,
  Query,
  UseGuards,
  Logger,
  BadRequestException,
} from '@nestjs/common';
import { isEmpty } from 'lodash';
import { ReqToken } from '../common/authentication/decorators';
import { AuthGuard } from '../common/authentication/guards';
import { ServiceToken } from '../common/authentication/service.token';
import { SourceToken } from '../common/authentication/source.token';
import { Token } from '../common/authentication/token';
import { UserToken } from '../common/authentication/user.token';
import { APIResponse } from '../common/response';
import { DeviceAuth } from './device.auth';
import { DeviceFilter, DeviceService } from './device.service';
import { CreateDeviceDTO } from './dto/createDevice.dto';
import { UpdateDeviceDTO } from './dto/updateDevice.dto';

function toNumber(str: string): number {
  try {
    return parseInt(str);
  } catch (e) {
    throw new BadRequestException();
  }
}

@UsePipes(new ValidationPipe({ whitelist: true }))
@Controller('devices')
export class DeviceController {
  constructor(private readonly service: DeviceService, private readonly auth: DeviceAuth) {}

  private parseQueryFilter(query: Record<string, string | string[] | undefined>): DeviceFilter {
    const filter: DeviceFilter = {
      sourceId: query.sourceId,
      typeId: query.typeId,
    };

    // Convert owner to number|number[]
    if (typeof query.owner !== 'undefined') {
      const owner = typeof query.owner === 'string' ? [query.owner] : query.owner;
      filter.owner = owner.map((o) => toNumber(o));
    }

    // Convert locationId to number|number[]
    if (typeof query.locationId == 'string') {
      filter.locationId = toNumber(query.locationId);
    } else if (Array.isArray(query.locationId)) {
      filter.locationId = query.locationId.map((o) => toNumber(o));
    }

    // Convert configuration
    if (typeof query.configuration === 'string') {
      try {
        filter.configuration = JSON.parse(query.configuration);
      } catch (e) {
        throw new BadRequestException();
      }
    }

    return filter;
  }

  @Get()
  @UseGuards(new AuthGuard([UserToken, SourceToken, ServiceToken]))
  getMany(@ReqToken() token: Token, @Query() query: any) {
    const filter = this.parseQueryFilter(query);
    this.auth.canGetMany(token, filter);

    return this.service.getMany(filter);
  }

  @Get(':id')
  @UseGuards(new AuthGuard([UserToken, SourceToken, ServiceToken]))
  async getOne(@ReqToken() token: Token, @Param('id', ParseIntPipe) id: number) {
    const device = await this.service.getOne(id);
    if (!device) {
      throw new NotFoundException();
    }
    this.auth.canGetOne(token, device);
    return device;
  }

  @Post()
  @UseGuards(new AuthGuard([UserToken]))
  createOne(@ReqToken() token: Token, @Body() dto: CreateDeviceDTO) {
    this.auth.canCreateOne(token, dto);
    return this.service.createOne(dto, dto.ownerId);
  }

  @Patch(':id')
  @UseGuards(new AuthGuard([UserToken]))
  async updateOne(@ReqToken() token: Token, @Param('id', ParseIntPipe) id: number, @Body() dto: UpdateDeviceDTO) {
    if (isEmpty(dto)) {
      // TODO: make 400 bad request
      return new APIResponse(200, 'Nothing to update');
    }

    const device = await this.service.getOne(id);
    if (!device) {
      throw new NotFoundException();
    }
    this.auth.canUpdateOne(token, device);
    return this.service.updateOne(device, dto);
  }

  @Delete(':id')
  @UseGuards(new AuthGuard([UserToken]))
  async deleteOne(@ReqToken() token: Token, @Param('id', ParseIntPipe) id: number) {
    const device = await this.service.getOne(id);
    if (!device) {
      throw new NotFoundException();
    }
    this.auth.canDeleteOne(token, device);
    return this.service.deleteOne(device);
  }
}
