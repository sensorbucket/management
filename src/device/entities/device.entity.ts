import { Location } from '../../location/entities/location.entity';
import { PrimaryGeneratedColumn, Entity, Column, JoinColumn, ManyToOne } from 'typeorm';
import { DeviceType } from '../../device-type/entities/deviceType.entity';
import { Source } from '../../source/entities/source.entity';

@Entity()
export class Device {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ length: 75 })
  code: string;

  @Column({ length: 128 })
  description: string;

  @ManyToOne((type) => DeviceType)
  @JoinColumn()
  type: DeviceType;

  @Column()
  typeId: string;

  @ManyToOne((type) => Source)
  @JoinColumn()
  source: Source;

  @Column()
  sourceId: string;

  @Column()
  ownerId: number;

  @ManyToOne((type) => Location)
  @JoinColumn()
  location: Location;

  @Column({ nullable: true })
  locationId: number;

  @Column('json')
  configuration: Record<string, any>;
}
