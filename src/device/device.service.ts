import { Injectable, BadRequestException, NotFoundException, Logger } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository, SelectQueryBuilder } from 'typeorm';
import * as Ajv from 'ajv';
import { keys } from 'lodash';
import { Device } from './entities/device.entity';
import { CreateDeviceDTO } from './dto/createDevice.dto';
import { UpdateDeviceDTO } from './dto/updateDevice.dto';
import { SourceService } from '../source/source.service';
import { DeviceTypeService } from '../device-type/device-type.service';
import { Source } from '../source/entities/source.entity';
import { DeviceType } from '../device-type/entities/deviceType.entity';
import { LocationService } from '../location/location.service';

interface Configuration {
  source: {};
  type: {};
}

export interface DeviceFilter {
  owner?: number | number[];
  sourceId?: string | string[];
  typeId?: string | string[];
  locationId?: number | number[];
  configuration?: { source: any; type: any };
}

function createPrimitiveFilter<T>(key: string) {
  return function (q: SelectQueryBuilder<T>, value: string | string[] | number | number[]) {
    if (Array.isArray(value)) {
      return q.andWhere(`"${key}" = ANY(:${key})`, { [key]: value });
    }
    return q.andWhere(`"${key}" = :${key}`, { [key]: value });
  };
}
function createJSONSubsetFilter<T>(key: string) {
  return function (q: SelectQueryBuilder<T>, value: Record<any, any>) {
    return q.andWhere(`"${key}" ::jsonb @> :${key}`, { [key]: value });
  };
}

@Injectable()
export class DeviceService {
  private readonly allowedFilters: Record<string, <T>(q: SelectQueryBuilder<T>, v: any) => any> = {
    sourceId: createPrimitiveFilter('sourceId'),
    typeId: createPrimitiveFilter('typeId'),
    locationId: createPrimitiveFilter('locationId'),
    owner: createPrimitiveFilter('owner'),
    configuration: createJSONSubsetFilter('configuration'),
  };

  constructor(
    @InjectRepository(Device) private readonly repo: Repository<Device>,
    private readonly sourceService: SourceService,
    private readonly deviceTypeService: DeviceTypeService,
    private readonly locationService: LocationService
  ) {}

  /**
   *
   * @param configuration
   * @param source
   * @param deviceType
   */
  private validateConfiguration(configuration: Configuration, source: Source, deviceType: DeviceType): string[] {
    let errors = [];
    const ajv = new Ajv();

    const schema = {
      additionalProperties: false,
      type: 'object',
      required: ['source', 'type'],
      properties: {
        source: source.configurationSchema,
        type: deviceType.configurationSchema,
      },
    };

    // Validate Source Configuration
    ajv.validate(schema, configuration);
    if (ajv.errors?.length) {
      errors.push(ajv.errorsText(ajv.errors, { dataVar: 'configuration' }));
    }

    return errors;
  }

  /**
   * Get all devices from joined organisations
   */
  async getMany(filter?: Record<string, any>): Promise<Device[]> {
    let query = this.repo.createQueryBuilder().select();

    // Apply filters
    if (filter) {
      // Loop properties and values
      for (const key in filter) {
        // If the property is not allowed skip it
        if (!keys(this.allowedFilters).includes(key)) {
          continue;
        }

        // Ensure it's not a prototype property
        if (filter.hasOwnProperty(key)) {
          const value = filter[key];
          if (value === undefined) continue;
          this.allowedFilters[key](query, value);
        }
      }
    }

    Logger.debug(query.getQueryAndParameters());

    return query.getMany();
  }

  /**
   *
   * @param id
   */
  async getOne(id: number): Promise<Device> {
    return this.repo.createQueryBuilder().select().where('id = :id', { id }).getOne();
  }

  /**
   *
   * @param dto
   * @param owner
   */
  async createOne(dto: CreateDeviceDTO, owner: number): Promise<Device> {
    // Source and DeviceType must exist
    const [source, type, location] = await Promise.all([
      this.sourceService.getOne(dto.source),
      this.deviceTypeService.getOne(dto.type),
      dto.locationId !== undefined ? this.locationService.getOne(dto.locationId) : Promise.resolve(null),
    ]);
    if (!source === undefined) {
      throw new NotFoundException('Source does not exist');
    }
    if (!type === undefined) {
      throw new NotFoundException('DeviceType does not exist');
    }
    if (!location === undefined) {
      throw new NotFoundException('Location does not exist');
    }

    // Validate configuration
    const errors = this.validateConfiguration(dto.configuration, source, type);
    if (errors.length > 0) {
      throw new BadRequestException(errors);
    }

    const result = await this.repo
      .createQueryBuilder()
      .insert()
      .values([
        {
          code: dto.code,
          description: dto.description,
          type: type,
          source: source,
          ownerId: owner,
          location: location,
          configuration: dto.configuration as any,
        },
      ])
      .returning('*')
      .execute();
    return this.getOne(result.generatedMaps[0].id);
  }

  /**
   *
   * @param device
   * @param dto
   */
  async updateOne(device: Device, dto: UpdateDeviceDTO): Promise<Device> {
    if (typeof device.id !== 'number') {
      throw new Error('Cannot update device without id!');
    }
    await this.repo
      .createQueryBuilder()
      .update()
      .set(dto as {})
      .where('id = :id', { id: device.id })
      .execute();
    return this.getOne(device.id);
  }

  /**
   *
   * @param device
   */
  async deleteOne(device: Device): Promise<void> {
    if (typeof device.id !== 'number') {
      throw new Error('Cannot delete device without id!');
    }
    const result = await this.repo.createQueryBuilder().delete().where('id = :id', { id: device.id }).execute();
  }
}
