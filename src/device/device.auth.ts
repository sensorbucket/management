import { ForbiddenException, Injectable, Logger, NotFoundException, UnauthorizedException } from '@nestjs/common';
import { SourceToken } from '../common/authentication/source.token';
import { AssertTokenType, Token } from '../common/authentication/token';
import { UserToken } from '../common/authentication/user.token';
import { DeviceFilter } from './device.service';
import { CreateDeviceDTO } from './dto/createDevice.dto';
import { Device } from './entities/device.entity';

@Injectable()
export class DeviceAuth {
  private readonly logger = new Logger(DeviceAuth.name);

  canGetMany(token: Token, filter: DeviceFilter) {
    if (Array.isArray(filter.owner)) {
      filter.owner.forEach((id) => {
        if (!token.canAccessOrganisation(id)) {
          throw new ForbiddenException('Not authorized for that organisation');
        }
      });
    } else if (typeof filter.owner === 'number') {
      if (!token.canAccessOrganisation(filter.owner)) {
        throw new ForbiddenException('Not authorized for that organisation');
      }
    }
  }

  canGetOne(token: Token, device: Device) {
    if (!token.canAccessOrganisation(device.ownerId)) {
      this.logger.log(`User tried requesting device that it has no access too, returning 404`);
      throw new NotFoundException();
    }
  }

  canCreateOne(token: Token, dto: CreateDeviceDTO) {
    if (!token.canAccessOrganisation(dto.ownerId)) {
      throw new UnauthorizedException('Cannot create device for that organisation');
    }
  }

  canUpdateOne(token: Token, device: Device) {
    if (!token.canAccessOrganisation(device.ownerId)) {
      this.logger.log(`User tried requesting device that it has no access too, returning 404`);
      throw new NotFoundException();
    }
  }

  canDeleteOne(token: Token, device: Device) {
    if (!token.canAccessOrganisation(device.ownerId)) {
      this.logger.log(`User tried requesting device that it has no access too, returning 404`);
      throw new NotFoundException();
    }
  }
}
