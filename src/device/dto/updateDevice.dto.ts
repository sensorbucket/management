import { IsObject, IsString, IsOptional, ValidateNested, MaxLength, IsNumber } from 'class-validator';
import { Type } from 'class-transformer';

class Configuration {
  @IsObject()
  @IsOptional()
  source?: {};

  @IsObject()
  @IsOptional()
  type?: {};
}

export class UpdateDeviceDTO {
  @IsString()
  @MaxLength(75)
  @IsOptional()
  code?: string;

  @IsString()
  @MaxLength(128)
  @IsOptional()
  description?: string;

  @IsNumber()
  @IsOptional()
  locationId?: number;

  @ValidateNested()
  @Type((type) => Configuration)
  @IsOptional()
  configuration?: Configuration;
}
