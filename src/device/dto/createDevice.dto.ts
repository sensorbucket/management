import { IsObject, IsString, IsNumber, ValidateNested, MaxLength, IsOptional } from 'class-validator';
import { Type } from 'class-transformer';

class Configuration {
  @IsObject()
  source: {};

  @IsObject()
  type: {};
}

export class CreateDeviceDTO {
  @IsString()
  @MaxLength(75)
  code: string;

  @IsNumber()
  ownerId: number;

  @IsString()
  description: string;

  @IsString()
  source: string;

  @IsString()
  type: string;

  @IsNumber()
  @IsOptional()
  locationId: number;

  @ValidateNested()
  @Type(() => Configuration)
  configuration: Configuration;
}
