export class APIError extends Error {
  httpStatus: number = 500;
  name: string = 'UNKNOWN_API_ERROR';
  constructor(message?: string) {
    super(message);
  }

  toString(): string {
    return `[${this.httpStatus} - ${this.name}] ${this.message}`;
  }
}
export class MalformedTokenError extends APIError {
  httpStatus = 401;
  name = 'AUTH_MALFORMED_TOKEN';
  message = 'The authentication could not be parsed';
}
