import { PageMetadata } from './decorators';

export class APIResponse {
  constructor(public status: number, public message: string, public data?: any) {}
}
export class APIPaginatedResponse extends APIResponse {
  constructor(status: number, message: string, data: any, public pagination: PageMetadata) {
    super(status, message, data);
  }
}
