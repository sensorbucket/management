import { Injectable, NestInterceptor, ExecutionContext, CallHandler } from '@nestjs/common';
import { Observable } from 'rxjs';
import { filter, map, tap } from 'rxjs/operators';
import { Request } from 'express';
import { APIResponse } from '../response';

@Injectable()
export class APIResponseInterceptor implements NestInterceptor {
  intercept(context: ExecutionContext, next: CallHandler): Observable<any> {
    let res = context.switchToHttp().getResponse() as Request;

    // Do not continue if not http
    if (res === null) {
      return next.handle();
    }

    return next.handle().pipe(
      // Force response to be of type APIResponse
      map((data) => {
        if (data instanceof APIResponse) {
          return data;
        }
        return new APIResponse(res.statusCode, undefined, data);
      }),
      // Set status code to API Response status code
      tap((data: APIResponse) => {
        res.statusCode = data.status;
      })
    );
  }
}
