import { IsJSONSchema } from './';
import { validate } from 'class-validator';
import { plainToClass } from 'class-transformer';

class TestClass {
  @IsJSONSchema()
  schema: {};
}

describe('JSONSchema validator', () => {
  beforeEach(async () => {});

  function cast(obj: Object): TestClass {
    return plainToClass(TestClass, obj);
  }

  it('Should return no errors on valid schema', async () => {
    const valid = cast({
      schema: {
        $schema: 'http://json-schema.org/draft-07/schema#',
        type: 'object',
        required: ['apples'],
        properties: {
          apples: {
            type: 'number',
            minimum: 5,
          },
        },
      },
    });
    const validationErrors = await validate(valid);
    expect(validationErrors).toHaveLength(0);
  });

  describe('Should throw on invalid schema...', () => {
    const schemas = [
      [
        'Incorrect base type',
        {
          $schema: 'http://json-schema.org/draft-07/schema#',
          type: 'abject', // Intentional misspelling
          required: ['apples'],
          properties: {
            apples: {
              type: 'number',
              minimum: 5,
            },
          },
        },
      ],
      [
        'Missing incorrect property type',
        {
          $schema: 'http://json-schema.org/draft-07/schema#',
          type: 'object',
          required: ['apples'],
          properties: {
            apples: {
              // Intentional misspelling
              type: 'nomber',
              minimum: 5,
            },
          },
        },
      ],
    ];

    it.each(schemas)('because %s', async (_, schema: any) => {
      const valid = cast({
        schema: schema,
      });
      const validationErrors = await validate(valid);
      expect(validationErrors.length).toBeGreaterThanOrEqual(1);
    });
  });
});
