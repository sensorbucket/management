import { Injectable, InternalServerErrorException, Logger } from '@nestjs/common';
import Axios, { AxiosInstance } from 'axios';
import { AppConfig } from '../../config/config.service';

interface JWK {
  kid: string;
}

interface JWKS {
  keys: JWK[];
}

@Injectable()
export class Keystore {
  private readonly logger = new Logger(Keystore.name, true);
  private readonly svc: AxiosInstance;

  constructor(config: AppConfig) {
    this.svc = Axios.create({
      baseURL: config.kmsSigning,
    });
  }

  async getKeystore(): Promise<JWKS> {
    try {
      const req = await this.svc.get<JWKS>('');
      return req.data;
    } catch (e) {
      this.logger.error('Could not fetch PEM Key: ', e);
      throw new InternalServerErrorException();
    }
  }

  async getLatestPEM() {
    const set = await this.getKeystore();

    // Must have at least one key
    if (set.keys.length === 0) {
      this.logger.error(`Keystore has no keys`);
      throw new InternalServerErrorException();
    }

    const kid = set.keys[0].kid;

    return {
      kid: kid,
      pem: await this.getPEM(kid),
    };
  }

  async getPEM(kid: string): Promise<string> {
    try {
      const req = await this.svc.get(`/${kid}?format=pem`);
      return req.data.data;
    } catch (e) {
      this.logger.error('Could not fetch PEM Key: ', e);
      throw new InternalServerErrorException();
    }
  }
}
