import * as jwt from 'jsonwebtoken';
import { MalformedTokenError } from '../errors';
import { InvalidTokenAudience, Token } from './token';
import { Injectable, Logger, NestMiddleware, UnauthorizedException } from '@nestjs/common';
import { SourceToken } from './source.token';
import { UserToken } from './user.token';
import { ServiceToken } from './service.token';
import { Keystore } from './keystore';

@Injectable()
export class AuthenticationMiddleware implements NestMiddleware {
  private readonly logger = new Logger(AuthenticationMiddleware.name);

  constructor(private readonly keystore: Keystore) {}

  /**
   * Verifies and decodes the JWT in a promisy way
   * @param tokenStr string representing the JWT
   * @returns
   */
  async parseJWT(tokenStr: string): Promise<any> {
    return new Promise((resolve, reject) => {
      jwt.verify(
        tokenStr,
        async (header, cb) => {
          try {
            const key = await this.keystore.getPEM(header.kid);
            cb(null, key);
          } catch (e) {
            return reject(e);
          }
        },
        {
          algorithms: ['RS256'],
        },
        (err, obj) => {
          if (err) {
            return reject(err);
          }
          return resolve(obj);
        }
      );
    });
  }

  async use(req: any, res: any, next: () => void) {
    const header: string = req.headers['authorization'] || null;

    // No auth header
    if (header === null) {
      req.token = null;
      return next();
    }

    const parts = header.split(' ');

    // Format only has two parts "Bearer <token>"
    if (parts.length != 2) {
      throw new MalformedTokenError();
    }

    // Only support bearer tokens
    if (parts[0].toLowerCase() !== 'bearer') {
      throw new MalformedTokenError('Authentication only supports bearer tokens.');
    }

    let token = null;
    try {
      token = await this.parseJWT(parts[1]);
    } catch (e) {
      this.logger.warn(`Could not parse JWT: ${e}`);
      throw new MalformedTokenError();
    }

    try {
      req.token = InstantiateTokenClass(token);
    } catch (e) {
      if (e instanceof InvalidTokenAudience) {
        throw new UnauthorizedException('Invalid token audience');
      }
      Logger.warn(`Token could not be created from object during authentication middleware: ${e}`);
      throw new UnauthorizedException('Token error occured');
    }

    next();
  }
}

/**
 * Create a token class from a JWT object
 */
export const InstantiateTokenClass = (obj: any): Token => {
  switch (obj.aud) {
    case 'user':
      return UserToken.fromObject(obj);
    case 'source':
      return SourceToken.fromObject(obj);
    case 'service':
      return ServiceToken.fromObject(obj);
    default:
      throw new InvalidTokenAudience();
  }
};
