import { Global, Module } from '@nestjs/common';
import { Keystore } from './keystore';

@Global()
@Module({
  providers: [Keystore],
  exports: [Keystore],
})
export class AuthModule {}
