import { Token, InvalidToken } from './token';

/**
 *
 */
export class ServiceToken extends Token {
  constructor(private readonly issuer: string, private readonly audience: string) {
    super();
  }

  public static fromObject({ aud, iss }: any): ServiceToken {
    if (typeof aud !== 'string') {
      throw new InvalidToken('Invalid `aud` claim in token');
    }
    if (typeof iss !== 'string') {
      throw new InvalidToken('Invalid `iss` claim in token');
    }
    return new ServiceToken(iss, aud);
  }

  canAccessOrganisation(id: number): boolean {
    return true;
  }

  isSuperUser(): boolean {
    return false;
  }
}
