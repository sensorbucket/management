import { Token, InvalidToken } from './token';

/**
 *
 */
export class SourceToken extends Token {
  constructor(private readonly subject: string, private readonly organisation: number) {
    super();
  }

  public static fromObject({ sub, org }: any): SourceToken {
    if (typeof sub !== 'string') {
      throw new InvalidToken('Invalid `sub` claim in token');
    }
    if (typeof org !== 'number') {
      throw new InvalidToken('Invalid `org` claim in token');
    }
    return new SourceToken(sub, org);
  }

  canAccessOrganisation(id: number): boolean {
    return this.organisation === id;
  }

  isSuperUser(): boolean {
    return false;
  }
}
