import { Token, InvalidToken } from './token';

/**
 *
 */
export class UserToken extends Token {
  constructor(
    private readonly userId: number,
    private readonly organisations: number[],
    private readonly superuser: boolean
  ) {
    super();
  }

  public static fromObject({ sub, orgs, su }: any): UserToken {
    let userId: number = null;
    let isSuperUser: boolean = false;

    // 'sub' claim must exist
    if (typeof sub !== 'string') {
      throw new InvalidToken(`User token has invalid 'sub' field`);
    }

    // 'sub' claim must represent an integer
    try {
      userId = parseInt(sub);
    } catch (e) {
      throw new InvalidToken(`User token has invalid 'sub' field`);
    }

    // 'orgs' claim must exist
    if (typeof orgs === undefined) {
      orgs = [];
    }
    if (!Array.isArray(orgs)) {
      throw new InvalidToken(`User token has invalid 'orgs' field`);
    }

    // Set superuser to true is su is of type boolean and is true
    if (su === true) {
      isSuperUser = true;
    }

    return new UserToken(userId, orgs, isSuperUser);
  }

  canAccessOrganisation(id: number): boolean {
    return this.superuser || this.organisations.includes(id);
  }

  isSuperUser(): boolean {
    return this.superuser;
  }
}
