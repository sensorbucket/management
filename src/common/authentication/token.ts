export type UnknownConstructor = new (..._: unknown[]) => unknown;

/**
 *
 */
export abstract class Token {
  abstract canAccessOrganisation(id: number): boolean;
  abstract isSuperUser(): boolean;
}

export class InvalidToken extends Error {}

export class InvalidTokenAudience extends InvalidToken {
  constructor() {
    super('Invalid token audience');
  }
}

export function AssertTokenType<I extends UnknownConstructor, U extends I[]>(
  t: InstanceType<I>,
  klasses: U
): InstanceType<U[number]> {
  for (let ix = 0; ix < klasses.length; ix++) {
    if (t instanceof klasses[ix]) {
      return t;
    }
  }
  return null;
}
