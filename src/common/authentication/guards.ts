import {
  CanActivate,
  ExecutionContext,
  ForbiddenException,
  Injectable,
  Logger,
  UnauthorizedException,
} from '@nestjs/common';
import { AssertTokenType, Token, UnknownConstructor } from './token';

@Injectable()
export class AuthGuard implements CanActivate {
  private readonly logger = new Logger(AuthGuard.name);
  constructor(private readonly klasses: UnknownConstructor[]) {}

  canActivate(context: ExecutionContext) {
    const request = context.switchToHttp().getRequest();
    return this.validateRequest(request);
  }

  isTokenOneOf(...ctors: UnknownConstructor[]): boolean {
    for (let ix = 0; ix < ctors.length; ix++) {
      if (this instanceof ctors[ix]) {
        return true;
      }
    }
    return false;
  }

  validateRequest(req: any): boolean {
    const token = req.token as Token;
    if (!token) {
      throw new UnauthorizedException('You need to be authorized to access this resource');
    }
    if (this.isTokenOneOf(...this.klasses)) {
      throw new ForbiddenException('Your authentication token is not meant to access this resource');
    }
    return true;
  }
}
