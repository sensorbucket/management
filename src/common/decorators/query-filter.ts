import { BadRequestException, createParamDecorator, ExecutionContext } from '@nestjs/common';
import { Request } from 'express';
import { intersection } from 'lodash';

function asNumber(name: string, v: any): number {
  try {
    return parseInt(v);
  } catch (e) {
    throw new BadRequestException(`${name} parameter must be an integer`);
  }
}
function asString(name: string, v: any): string {
  if (typeof v === 'string') {
    return v;
  }
  throw new BadRequestException(`${name} parameter must be a string`);
}

export interface QueryFilterExtractorOpts {
  limit?: number;
  maxLimit?: number;
  filterFields?: string[];
}
export interface QueryFilter {
  limit: number;
  page: number;
  filters: Record<string, any>;
  sortOrder: 'ASC' | 'DESC';
  sortBy: string | null;
}

export interface PageMetadata {
  page: number;
  limit: number;
  next?: string;
  previous?: string;
}

const DEFAULT_OPTS: QueryFilterExtractorOpts = {
  limit: 10,
  maxLimit: 100,
  filterFields: [],
};
export const CreateQueryFilterExtractor = (userOpts: QueryFilterExtractorOpts) => {
  const opts: QueryFilterExtractorOpts = {
    ...DEFAULT_OPTS,
    ...userOpts,
  };
  return createParamDecorator(
    (_, ctx: ExecutionContext): QueryFilter => {
      const req = ctx.switchToHttp().getRequest() as Request;
      const q = req.query;

      // Setup defaults
      const qf: QueryFilter = {
        limit: opts.limit,
        page: 0,
        filters: {},
        sortBy: null,
        sortOrder: 'ASC',
      };

      // Extract from query
      if (q.limit) {
        qf.limit = asNumber('limit', q.limit);
        // XXX: Should we perhaps return an error instead?
        if (qf.limit > opts.maxLimit) {
          qf.limit = opts.maxLimit;
        }
      }

      if (q.page) {
        qf.page = asNumber('page', q.page);
      }

      if (q.sortBy) {
        qf.sortBy = asString('sortBy', q.sortBy);
      }

      // SortOrder must be either ASC or DESC
      if (q.sortOrder) {
        if (!['ASC', 'DESC'].includes(asString('sortOrder', q.sortOrder).toUpperCase())) {
          throw new BadRequestException('SortOrder must be either ASC or DESC');
        }
        qf.sortOrder = q.sortOrder as any;
      }

      // Find filter parameters
      const filterFields = intersection(Object.keys(q), opts.filterFields);
      if (filterFields.length > 0) {
        qf.filters = filterFields.reduce(
          (result, fieldName) => ({ ...result, [fieldName]: asString(fieldName, q[fieldName]) }),
          {}
        );
      }

      return qf;
    }
  );
};
