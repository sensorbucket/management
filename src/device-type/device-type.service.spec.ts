import { Test, TestingModule } from '@nestjs/testing';
import { DeviceTypeService } from './device-type.service';
import { getRepositoryToken } from '@nestjs/typeorm';
import { DeviceType } from './entities/deviceType.entity';

const repositoryMock = {};

describe('DeviceTypeService', () => {
  let service: DeviceTypeService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        {
          provide: getRepositoryToken(DeviceType),
          useValue: repositoryMock,
        },
        DeviceTypeService,
      ],
    }).compile();

    service = module.get<DeviceTypeService>(DeviceTypeService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
