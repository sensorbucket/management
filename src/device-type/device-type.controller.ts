import {
  Controller,
  Get,
  Param,
  NotFoundException,
  Post,
  Body,
  Patch,
  Delete,
  UsePipes,
  ValidationPipe,
  UseGuards,
} from '@nestjs/common';
import { ReqToken } from '../common/authentication/decorators';
import { AuthGuard } from '../common/authentication/guards';
import { ServiceToken } from '../common/authentication/service.token';
import { Token } from '../common/authentication/token';
import { UserToken } from '../common/authentication/user.token';
import { DeviceTypeAuth } from './device-type.auth';
import { DeviceTypeService } from './device-type.service';
import { CreateDeviceTypeDTO } from './dto/createDeviceType.dto';
import { UpdateDeviceTypeDTO } from './dto/updateDeviceType.dto';

@UsePipes(new ValidationPipe({ whitelist: true }))
@Controller('device-types')
export class DeviceTypeController {
  constructor(private readonly service: DeviceTypeService, private readonly auth: DeviceTypeAuth) {}

  @Get()
  @UseGuards(new AuthGuard([UserToken, ServiceToken]))
  getMany() {
    return this.service.getMany();
  }

  @Get(':id')
  @UseGuards(new AuthGuard([UserToken, ServiceToken]))
  async getOne(@Param('id') id: string) {
    const devicetype = await this.service.getOne(id);
    if (!devicetype) {
      throw new NotFoundException();
    }
    return devicetype;
  }

  @Post()
  @UseGuards(new AuthGuard([UserToken]))
  createOne(@ReqToken() token: Token, @Body() dto: CreateDeviceTypeDTO) {
    this.auth.canCreateOne(token);
    return this.service.createOne(dto);
  }

  @Patch(':id')
  @UseGuards(new AuthGuard([UserToken]))
  async updateOne(@ReqToken() token: Token, @Param('id') id: string, @Body() dto: UpdateDeviceTypeDTO) {
    this.auth.canUpdateOne(token);
    const deviceType = await this.service.getOne(id);
    if (!deviceType) {
      throw new NotFoundException();
    }
    return this.service.updateOne(deviceType, dto);
  }

  @Delete(':id')
  @UseGuards(new AuthGuard([UserToken]))
  async deleteOne(@ReqToken() token: Token, @Param('id') id: string) {
    this.auth.canDeleteOne(token);
    const devicetype = await this.service.getOne(id);
    if (!devicetype) {
      throw new NotFoundException();
    }
    return this.service.deleteOne(devicetype);
  }
}
