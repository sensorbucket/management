import { Entity, Column, PrimaryColumn } from 'typeorm';

@Entity()
export class DeviceType {
  @PrimaryColumn()
  id: string;

  @Column({ length: 50 })
  name: string;

  @Column({ length: 75 })
  description: string;

  @Column()
  mobile: boolean;

  @Column({ type: 'json' })
  configurationSchema: {};
}
