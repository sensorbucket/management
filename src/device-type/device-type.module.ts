import { Module } from '@nestjs/common';
import { DeviceTypeService } from './device-type.service';
import { DeviceTypeController } from './device-type.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { DeviceType } from './entities/deviceType.entity';
import { DeviceTypeAuth } from './device-type.auth';

@Module({
  imports: [TypeOrmModule.forFeature([DeviceType])],
  providers: [DeviceTypeAuth, DeviceTypeService],
  controllers: [DeviceTypeController],
  exports: [DeviceTypeService],
})
export class DeviceTypeModule {}
