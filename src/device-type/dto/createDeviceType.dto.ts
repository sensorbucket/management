import { IsString, IsOptional, MinLength, MaxLength, maxLength, IsBoolean } from 'class-validator';
import { IsJSONSchema } from '../../common/validators';

export class CreateDeviceTypeDTO {
  // If id is not given it will be derived from the name
  // where space become dashes, etc.
  @IsString()
  @MinLength(3)
  @MaxLength(50)
  @IsOptional()
  id?: string;

  @IsString()
  @MinLength(3)
  @MaxLength(50)
  name: string;

  @IsString()
  @MaxLength(75)
  @IsOptional()
  description: string;

  @IsBoolean()
  mobile: boolean;

  @IsJSONSchema()
  configurationSchema: string | { [index: string]: any };
}
