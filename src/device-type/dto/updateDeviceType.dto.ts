import { IsString, MinLength, MaxLength, IsOptional, IsBoolean } from 'class-validator';
import { IsJSONSchema } from '../../common/validators';

export class UpdateDeviceTypeDTO {
  @IsString()
  @MinLength(3)
  @MaxLength(50)
  @IsOptional()
  name: string;

  @IsString()
  @MaxLength(75)
  @IsOptional()
  @IsOptional()
  description: string;

  @IsBoolean()
  @IsOptional()
  mobile: boolean;

  @IsJSONSchema()
  @IsOptional()
  configurationSchema?: string | { [index: string]: any };
}
