import { Injectable, BadRequestException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { DeviceType } from './entities/deviceType.entity';
import { Repository } from 'typeorm';
import { CreateDeviceTypeDTO } from './dto/createDeviceType.dto';
import { UpdateDeviceTypeDTO } from './dto/updateDeviceType.dto';
import * as Ajv from 'ajv';

@Injectable()
export class DeviceTypeService {
  private readonly ajv: Ajv.Ajv;

  constructor(
    @InjectRepository(DeviceType) private readonly repo: Repository<DeviceType>,
  ) {
    this.ajv = Ajv();
  }

  private validateConfigurationSchema(schema: {}): Ajv.ErrorObject[] {
    this.ajv.validateSchema(schema);
    return this.ajv.errors;
  }

  private hasPrimaryKeys(obj: { id: string }): boolean {
    return typeof obj.id === 'string';
  }

  async getMany(): Promise<DeviceType[]> {
    return this.repo.createQueryBuilder().select().getMany();
  }

  async getManyVersions(id: string): Promise<DeviceType[]> {
    return this.repo
      .createQueryBuilder()
      .select()
      .where('id = :id', { id })
      .getMany();
  }

  async getOne(id: string): Promise<DeviceType> {
    return this.repo
      .createQueryBuilder()
      .select()
      .where('id = :id', { id })
      .getOne();
  }

  async createOne(_dto: CreateDeviceTypeDTO): Promise<DeviceType> {
    const dto = { ..._dto };
    // ConfigurationSchema must be a valid Draft #7 JSON Schema
    const errors = this.validateConfigurationSchema(dto.configurationSchema);
    if (errors && errors.length > 0) {
      throw new BadRequestException(
        { errors },
        'ConfigurationSchema is not a valid JSON Schema',
      );
    }

    if (!dto.id) {
      dto.id = dto.name
        .toLocaleLowerCase()
        .replace(/[^a-z0-9\s]/i, '')
        .replace(' ', '-');
    }

    // No duplicate id allowed
    const duplicate = await this.getOne(dto.id);
    if (duplicate) {
      throw new BadRequestException('DeviceType with that id already exists');
    }

    // Create devicetype
    const result = await this.repo
      .createQueryBuilder()
      .insert()
      .values([dto as {}])
      .returning('*')
      .execute();
    return this.getOne(dto.id);
  }

  async updateOne(
    deviceType: DeviceType,
    dto: UpdateDeviceTypeDTO,
  ): Promise<DeviceType> {
    if (!this.hasPrimaryKeys(deviceType)) {
      throw new Error('Cannot update DeviceType without id!');
    }
    const { id } = deviceType;
    await this.repo
      .createQueryBuilder()
      .update()
      .set(dto as {})
      .where('id = :id', { id })
      .execute();
    return this.getOne(id);
  }

  async deleteOne(deviceType: DeviceType): Promise<void> {
    if (!this.hasPrimaryKeys(deviceType)) {
      throw new Error('Cannot update devicetype without id!');
    }
    const result = await this.repo
      .createQueryBuilder()
      .delete()
      .where('id = :id', { id: deviceType.id })
      .execute();
  }
}
