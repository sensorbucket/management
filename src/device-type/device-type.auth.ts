import { ForbiddenException, Injectable, UnauthorizedException } from '@nestjs/common';
import { AssertTokenType, Token } from '../common/authentication/token';
import { UserToken } from '../common/authentication/user.token';

@Injectable()
export class DeviceTypeAuth {
  canCreateOne(token: Token) {
    if (!token.isSuperUser()) {
      throw new ForbiddenException();
    }
  }

  canUpdateOne(token: Token) {
    if (!token.isSuperUser()) {
      throw new ForbiddenException();
    }
  }

  canDeleteOne(token: Token) {
    if (!token.isSuperUser()) {
      throw new ForbiddenException();
    }
  }
}
